$(document).ready(function() {

    $(window).on('load', function() {
        $('body').addClass('load');
    })

    // mobile menu
    $('.mobile-btn').on('click', function() {
        $('body').addClass('noscroll');
        $('.nav').addClass('active');
    });
    $('.close-menu').on('click', function() {
        $('body').removeClass('noscroll');
        $('.nav').removeClass('active');
    });
    $('.sm-scroll').on('click', function() {
        $('body').removeClass('noscroll');
        $('.nav').removeClass('active');
    });


    /* Sticky menu */
    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 140) {
            $('.header').addClass('sticky-header');
        } else {
            $('.header').removeClass('sticky-header');
        }
    });


    // Background (установка background в html через атрибут "data-bg")
    var bgSelector = $(".bg-img");
    bgSelector.each(function(index, elem) {
        var element = $(elem),
            bgSource = element.data('bg');
        element.css('background-image', 'url(' + bgSource + ')');
    });


    // Выпадающий список
    $('.select').on('click', '.placeholder', function() {
        var parent = $(this).closest('.select');
        if (!parent.hasClass('is-open')) {
            parent.addClass('is-open');
            $('.select.is-open').not(parent).removeClass('is-open');
        } else {
            parent.removeClass('is-open');
        }
    }).on('click', 'ul>li', function() {
        var parent = $(this).closest('.select');
        parent.removeClass('is-open').find('.placeholder').text($(this).text());
        parent.find('input[type=hidden]').attr('value', $(this).attr('data-value'));
    });
    // -- Закрываем селект при клике вне элемента и при скролинге
    $(document).mouseup(function(e) {
        var div = $(".select"); //класс элемента вне которого клик
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            div.removeClass('is-open');
        }
    });
    $(document).scroll(function(e) {
        var div = $(".select");
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            div.removeClass('is-open');
        }
    });


    // Плавная прокрутка
    $(document).ready(function() {
        $('.sm-scroll').click(function() {
            var el = $(this).attr('href');
            el = el.replace(/[^\#]*/, '');
            $('body,html').animate({
                scrollTop: $(el).offset().top - 50
            }, 1000);
            return false;
        });
    });


    // Action slider
    $('.action-slider').slick({
        prevArrow: '<span class="prev-slide"><svg width="21" height="16" viewBox="0 0 21 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20.7071 8.70711C21.0976 8.31658 21.0976 7.68342 20.7071 7.29289L14.3431 0.928932C13.9526 0.538408 13.3195 0.538408 12.9289 0.928932C12.5384 1.31946 12.5384 1.95262 12.9289 2.34315L18.5858 8L12.9289 13.6569C12.5384 14.0474 12.5384 14.6805 12.9289 15.0711C13.3195 15.4616 13.9526 15.4616 14.3431 15.0711L20.7071 8.70711ZM0 9H20V7H0V9Z" fill="currentColor"/></svg></span>',
        nextArrow: '<span class="next-slide"><svg width="21" height="16" viewBox="0 0 21 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20.7071 8.70711C21.0976 8.31658 21.0976 7.68342 20.7071 7.29289L14.3431 0.928932C13.9526 0.538408 13.3195 0.538408 12.9289 0.928932C12.5384 1.31946 12.5384 1.95262 12.9289 2.34315L18.5858 8L12.9289 13.6569C12.5384 14.0474 12.5384 14.6805 12.9289 15.0711C13.3195 15.4616 13.9526 15.4616 14.3431 15.0711L20.7071 8.70711ZM0 9H20V7H0V9Z" fill="currentColor"/></svg></span>',
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        dots: false,
        arrows: true,
        autoplay: false,
        autoplaySpeed: 6000,
    });


    // Gallery slider
    $('.gallery-slider').slick({
        appendArrows: '.gallery-slider-arrows',
        prevArrow: '<span class="prev-slide"><svg width="21" height="16" viewBox="0 0 21 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20.7071 8.70711C21.0976 8.31658 21.0976 7.68342 20.7071 7.29289L14.3431 0.928932C13.9526 0.538408 13.3195 0.538408 12.9289 0.928932C12.5384 1.31946 12.5384 1.95262 12.9289 2.34315L18.5858 8L12.9289 13.6569C12.5384 14.0474 12.5384 14.6805 12.9289 15.0711C13.3195 15.4616 13.9526 15.4616 14.3431 15.0711L20.7071 8.70711ZM0 9H20V7H0V9Z" fill="currentColor"/></svg></span>',
        nextArrow: '<span class="next-slide"><svg width="21" height="16" viewBox="0 0 21 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20.7071 8.70711C21.0976 8.31658 21.0976 7.68342 20.7071 7.29289L14.3431 0.928932C13.9526 0.538408 13.3195 0.538408 12.9289 0.928932C12.5384 1.31946 12.5384 1.95262 12.9289 2.34315L18.5858 8L12.9289 13.6569C12.5384 14.0474 12.5384 14.6805 12.9289 15.0711C13.3195 15.4616 13.9526 15.4616 14.3431 15.0711L20.7071 8.70711ZM0 9H20V7H0V9Z" fill="currentColor"/></svg></span>',
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        centerMode: true,

        responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    initialSlide: 1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    initialSlide: 1,
                }
            }
        ]
    });


    // Testimonial slider
    $('.testimonial-slider').slick({
        prevArrow: '<span class="prev-slide"><svg width="21" height="16" viewBox="0 0 21 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20.7071 8.70711C21.0976 8.31658 21.0976 7.68342 20.7071 7.29289L14.3431 0.928932C13.9526 0.538408 13.3195 0.538408 12.9289 0.928932C12.5384 1.31946 12.5384 1.95262 12.9289 2.34315L18.5858 8L12.9289 13.6569C12.5384 14.0474 12.5384 14.6805 12.9289 15.0711C13.3195 15.4616 13.9526 15.4616 14.3431 15.0711L20.7071 8.70711ZM0 9H20V7H0V9Z" fill="currentColor"/></svg></span>',
        nextArrow: '<span class="next-slide"><svg width="21" height="16" viewBox="0 0 21 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20.7071 8.70711C21.0976 8.31658 21.0976 7.68342 20.7071 7.29289L14.3431 0.928932C13.9526 0.538408 13.3195 0.538408 12.9289 0.928932C12.5384 1.31946 12.5384 1.95262 12.9289 2.34315L18.5858 8L12.9289 13.6569C12.5384 14.0474 12.5384 14.6805 12.9289 15.0711C13.3195 15.4616 13.9526 15.4616 14.3431 15.0711L20.7071 8.70711ZM0 9H20V7H0V9Z" fill="currentColor"/></svg></span>',
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 6000,
        responsive: [{
                breakpoint: 769,
                settings: {
                    slidesToShow: 2,
                    variableWidth: true,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    variableWidth: true,
                }
            }
        ]
    });


    // Popup
    $('.js--popup').on('click', function(e) {
        e.preventDefault();
        let btn = $(this).data('modal');
        $('#' + btn).addClass('active');
        $('.popup-overlay').show();
        $('body').toggleClass('noscroll');
    })

    $('.close-popup, .popup-overlay, .close-popup-btn').on('click', function(e) {
        e.preventDefault();
        $('.popup-overlay').hide();
        $('.popup-window').removeClass('active');
        $('body').removeClass('noscroll');
    })

    // Скрыть/Показать блоки в мобильной версии
    $('.more-btn').click(function(e) {
        e.preventDefault();
        $(this).closest('.service-wrapper').toggleClass('active');
        $(this).toggleClass('active');
        $('.more-btn').not(this).closest('.service-wrapper').removeClass('active');
    });

    // показывать номер телефона по клику на мобильном
    if ($(window).width() < 1200) {
        $('.header-item .call').on('click', function() {
            $(this).toggleClass('active');
            $('.mobile-phone-dropdown').slideToggle();
        })
    }

})